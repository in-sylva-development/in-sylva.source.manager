'use strict'

const KeycloakConnect = require('@pixelygroup/keycloak-koa-connect').default
const bodyStore = require('@pixelygroup/keycloak-koa-connect/stores/body-store').default // If this option is used, it is legal to include the value of jwt in the body
const queryStore = require('@pixelygroup/keycloak-koa-connect/stores/query-store').default // If this option is used, it is also legal to pass a token at http://a.com?jwt=token

const Keycloak = require('./keycloak.js')

console.log(Keycloak)

const keycloak = new KeycloakConnect({ store: [queryStore, bodyStore,]}, Keycloak)

module.exports = { keycloak }
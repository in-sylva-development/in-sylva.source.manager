'use strict'

module.exports = {

    'realm': process.env.KEYCLOAK_REALM, 
    'auth-server-url': process.env.KEYCLOAK_SERVER_URL,
    'ssl-required': 'external',
    'resource': process.env.KEYCLOAK_CLIENT_ID,
    'bearer-only': true,
    'credentials': {
        'secret': ''
    },
    'use-resource-role-mappings': true,
    'confidential-port': 0,
    'realm-public-key': ''
}
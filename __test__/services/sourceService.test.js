'use strict'

const { generateBlurredSites } = require('../../app/dal/sourceService')

/* test on kibana console. 
   GET source1-5f5d1131341b819359901df4/_search
{
  "_source": {
    "includes": [ "experimental_site.*" ]
  },
  "query": { 
        "bool": {
         "filter": {
                "term": {
                   "_id": "AdWLg3QBT3PiIp29Oq_K"
                }
            }
        }
    }
}
*/

describe("sourceService", () => {
    it("generateBlurredSites", async () => {
        const result = await generateBlurredSites()

        console.log(result)
    },40000)
})
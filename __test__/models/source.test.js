'use strict'

const { Source, Indices } = require('../../app/models/Source');

describe("Testing the source entity with Sequelize orm", () => {
    it("tests source entity", async () => {

        await Source.create({
            name: 'test',
            description: 'test',
            indices: [
                {
                    index_id: "test-2",
                    mng_id: "test-3"
                },
                {
                    index_id: "test-4",
                    mng_id: "test-5"
                },
                {
                    index_id: "test-6",
                    mng_id: "test-7"
                },
            ]
        }, {
            include: [{
                association: Indices,
                as: 'indices'
            }]
        }
        ).then(c => console.log(c))
    });
});
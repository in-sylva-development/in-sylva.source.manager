'use strict'

const { UnBlurredSite } = require('../../app/models/UnBlurredSites')

const json = require('./metadataRecordsFromPlantaExp.json')

describe("Test UnBlurredSite", () => {
    test("Insert UnBlurredSite", async () => {

        for (const metadata of json.metadataRecords) {

            if (metadata.experimental_site.geo_point.latitude && metadata.experimental_site.geo_point.longitude) {
                await UnBlurredSite.create({
                    userid: 1,
                    x: metadata.experimental_site.geo_point.latitude,
                    y: metadata.experimental_site.geo_point.longitude,
                    blurring_rule: '0.1;0.15'
                })
            }
        }
        
    })
})
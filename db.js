'use strict'

const MongoClient = require('mongodb').MongoClient
const { Client } = require("@elastic/elasticsearch")

// const assert = require('assert')
const configM = require('./app/config/mongo')
const configElk = require('./app/config/elk')
const configPg = require('./app/config/pg')
const knex = require('knex')
const Sequelize = require('sequelize')

const user = encodeURIComponent(configM.username)
const password = encodeURIComponent(configM.password)

const authMechanism = 'DEFAULT';

const url = `mongodb://${user}:${password}@${configM.host}:${configM.port}/?authMechanism=${authMechanism}`;

const clientPgSeq = new Sequelize(configPg.connection.database, configPg.connection.user, configPg.connection.password, {
  host: configPg.connection.host,
  port: configPg.connection.port,
  pool: configPg.pool,
  dialect: 'postgres',
  define: {
    createdAt: "createdat",
    updatedAt: "updatedat"
  },
})

/* pg connection check. 
clientPgSeq
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
}); */

const clientElk = new Client({
  node: `http://${configElk.host}:${configElk.port}`,
  maxRetries: 5,
  requestTimeout: 60000,
  sniffOnStart: true,
  auth: {
    username: configElk.username,
    password: configElk.password
  },
})

const clientM = new MongoClient(url, { useUnifiedTopology: true });
const clientPg = knex(configPg)

module.exports = {
  mongoC: clientM,
  postgresC: clientPg,
  postgresSeq: clientPgSeq,
  elkC: clientElk,
}
const Consul = require('consul');

class ConsulConfig {
    constructor () {
        //Initialize consumer
        this.consul = new Consul({
            host: this.ConsulIp,
            port: this.ConsulPort,
            promisify: true,
        });
        
        //Service registration and health check configuration
        this.consul.agent.service.register({
            name: this.ServiceName,
            Address: this.ServiceIp,
            port: this.ServicePort,
            /* check: {
                http: 'http://147.100.18.116:4000/healthcheck',
                interval: '10s',
                timeout: '6s', 
            } */
        }, function(err, result) {
            if (err) {
                console.error(err);
                throw err;
            }

            console.log('in-sylva.gatekeeper' + 'registered successfully! ')
        })
    }
    
    async getConfig(key) {
        const result = await this.consul.kv.get(key);

        if (!result) {
            return Promise.reject (key + 'does not exist');
        }

        return JSON.parse(result.Value);
    }
    
    //Read user configuration simple package
    async getUserConfig(key) {
        const result = await this.getConfig('develop/user');

        if (!key) {
            return result;
        }

        return result[key];
    }

    //Update user configuration simple package
    async setUserConfig(key, val) {
        const user = await this.getConfig('develop/user');

        user[key] = val;

        return this.consul.kv.set('develop/user', JSON.stringify(user))
    }
}

ConsulConfig.prototype.ConsulIp = null;
ConsulConfig.prototype.ConsulPort = null;
ConsulConfig.prototype.ServiceIp = null;
ConsulConfig.prototype.ServicePort = null;
ConsulConfig.prototype.ServiceName = 'in-sylva-source-manager';

module.exports = ConsulConfig;
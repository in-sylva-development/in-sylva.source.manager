'use strict'

const Koa = require('koa')
const logger = require('winston')
const api = require('./app/api')
const koaLogger = require('koa-logger')
const cors = require('koa2-cors')
const jwt = require('koa-jwt')
const {keycloak} = require('./init-keycloak');
const app = new Koa()

app.proxy = true

keycloak.middleware()
  .map(item => {
    app.use(item)
})

app
    .use(cors({
        origin: '*'
    }))
    .use(koaLogger())
    .use(function (ctx, next) {
        return next().catch((err) => {
            if (err) {
                ctx.status = 401
                ctx.body = err //'Protected resource, use Authorization header to get access\n'
            } else {
                /* */
            }
        })
    })
    .use(api.routes())
    .use(api.allowedMethods())
    //.use(jwt({ secret: 'shared-secret' }))
app.on('error', (err) => {
    logger.error('Server error', { error: err.message })
    throw err
})

module.exports = app
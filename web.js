'use strict'

const { promisify } = require('util')
const http = require('http')
const logger = require('winston')
const app = require('./server')
const config = require('./app/config/server')

logger.level = config.logger.level

const server = http.createServer(app.callback())
const serverListen = promisify(server.listen).bind(server)

// const ConsulConfig = require('./consul');
// const consul = new ConsulConfig();
// consul.ConsulIp = process.env.CONSUL_IP
// consul.ConsulPort = process.env.CONSUL_PORT
// consul.ServiceIp = process.env.SERVICE_IP
// consul.ServicePort = process.env.PORT
// consul.ServiceName = process.env.SERVICE_NAME

serverListen(config.port)
    .then(() => {
        logger.info(`in-sylva.source.manager service is up and running on localhost:${config.port}`)
    })
    .catch((err) => {
        logger.error(err)
        process.exit(1)
    })
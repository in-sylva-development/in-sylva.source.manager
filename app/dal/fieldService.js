'use strict'

const { HttpClient, BotService } = require('@in-sylva/common')
const { mongoC, postgresC } = require('../../db')

const gatekeeperUrl = process.env.IIN_SYLVA_GATEKEEPER_HOST
const port = process.env.IIN_SYLVA_GATEKEEPER_PORT

const gatekeeperClient = new HttpClient();
gatekeeperClient.baseUrl = `${gatekeeperUrl}:${port}`

const botService = new BotService();
botService.token = process.env.BOT_SERVICE_TOKEN
botService.channel = process.env.BOT_SERVICE_CHANNEL

const { StdField } = require('../models/StdField')

const FieldService = {

    async createAddtl(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const addtlField = await postgresC('addtl_fields').insert({
                category: ctx.category,
                field_name: ctx.field_name,
                definition_and_comment: ctx.definition_and_comment,
                obligation_or_condition: ctx.obligation_or_condition,
                field_type: ctx.field_type,
                values: ctx.values,
                cardinality: ctx.cardinality,
                ispublic: ctx.ispublic,
                isoptional: ctx.isoptional
            }).returning('*').then((result) => result[0])

            return addtlField

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },
    async createStd(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const stdField = await StdField.create({
                category: ctx.category,
                field_name: ctx.field_name,
                definition_and_comment: ctx.definition_and_comment,
                obligation_or_condition: ctx.obligation_or_condition,
                field_type: ctx.field_type,
                cardinality: ctx.cardinality,
                values: ctx.values,
                ispublic: ctx.ispublic,
                isoptional: ctx.isoptional
            })

            return stdField

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },
    async updateAddtl(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const result = await postgresC('addtl_fields').where('id', ctx.id).update({
                category: ctx.category,
                field_name: ctx.field_name,
                definition_and_comment: ctx.definition_and_comment,
                obligation_or_condition: ctx.obligation_or_condition,
                field_type: ctx.field_type,
                cardinality: ctx.cardinality,
                values: ctx.values,
                ispublic: ctx.ispublic,
                isoptional: ctx.isoptional
            }).returning('*').then((result) => result[0])

            return result

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },
    async updateStd(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const result = await postgresC('std_fields').where('id', ctx.id).update({
                category: ctx.category,
                field_name: ctx.field_name,
                definition_and_comment: ctx.definition_and_comment,
                obligation_or_condition: ctx.obligation_or_condition,
                cardinality: ctx.cardinality,
                field_type: ctx.field_type,
                values: ctx.values,
                ispublic: ctx.ispublic,
                isoptional: ctx.isoptional
            }).returning('*').then((result) => result[0])

            return result

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },
    async truncateStdField(ctx) {
        try {
            return await postgresC.raw("TRUNCATE std_fields,policy_fields RESTART IDENTITY CASCADE;").then((result) => result[0])
        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async deleteAddtl(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const result = await postgresC.select().from('addtl_fields').where('id', ctx.id).timeout(1000, { cancel: true })
            return result

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },
    async deleteStd(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const result = await postgresC.select().from('std_fields').where('id', ctx.id).timeout(1000, { cancel: true })
            return result
        } catch (error) {
            botService.message(`Caught error: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async addtlFields(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }


        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async stdFields(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }
            /*
            const roles = []
            if (ctx.kc_id) {
                const user = await gatekeeperClient.post(`user/detail`, { kcId: ctx.kc_id })
                if (user) {
                    const allocatedRoles = await gatekeeperClient.get(`/allocatedRoles`, { kc_id: user.content.kc_id })

                    return allocatedRoles
                }
            }*/

            const stdFields = await postgresC.select().from('std_fields').timeout(1000, { cancel: true })
            return stdFields;

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },


    async privateFieldList(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }
            return await StdField.findAll({ where: { ispublic: false } })

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async publicFieldList(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }
            return await StdField.findAll({ where: { ispublic: true } })

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },


    async policyFields(ctx) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const params = { userId: ctx.userId }


            const policyFieldList = await postgresC.raw(`
                select distinct sf.id as std_id, sf.field_name, s.name as sources_name,p.name as policy_name,s.id as source_id, sf.field_type, sf.definition_and_comment,sf.obligation_or_condition ,sf.cardinality, sf.category, sf.values from groups g
                inner join group_users gu on g.id = gu.group_id
                inner join groups_policies gp on g.id = gp.group_id
                inner join policies p on gp.policy_id = p.id
                inner join policy_sources ps on p.id = ps.policy_id
                inner join sources s on s.id = ps.source_id
                inner join policy_fields pf on p.id = pf.policy_id
                inner join std_fields sf on sf.id = pf.std_field_id
                inner join users u on gu.user_id = u.id
                
                where u.kc_id = :userId and sf.ispublic = false
            `, params)

            return policyFieldList.rows

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    }
}

module.exports = {
    createAddtl: FieldService.createAddtl,
    createStd: FieldService.createStd,
    updateAddtl: FieldService.updateAddtl,
    updateStd: FieldService.updateStd,
    deleteAddtl: FieldService.deleteAddtl,
    deleteStd: FieldService.deleteStd,
    addtlFields: FieldService.addtlFields,
    stdFields: FieldService.stdFields,
    privateFieldList: FieldService.privateFieldList,
    publicFieldList: FieldService.publicFieldList,
    policyFields: FieldService.policyFields,
    truncateStdField: FieldService.truncateStdField
}



'use strict'

const { mongoC, postgresC, elkC } = require('../../db')

const configM = require('../config/mongo')
require("array.prototype.flatmap").shim()

const { HttpClient, BotService } = require('@in-sylva/common')

const gatekeeperUrl = process.env.IN_SYLVA_GATEKEEPER_HOST
const port = process.env.IN_SYLVA_GATEKEEPER_PORT

const gatekeeperClient = new HttpClient()
gatekeeperClient.baseUrl = `${gatekeeperUrl}:${port}/`

const botService = new BotService();
botService.token = process.env.BOT_SERVICE_TOKEN
botService.channel = process.env.BOT_SERVICE_CHANNEL

const { ProviderSource } = require('../models/ProviderSource')
const { Source, SourceIndices } = require('../models/Source')

const { UnBlurredSite } = require('../models/UnBlurredSites')

const deleteElkIndex = async (indexId) => {
    try {
        await elkC.indices.delete({
            index: indexId,
        }).then(function (resp) {
            console.log(JSON.stringify(resp, null, 4))
        }, function (err) {
            console.trace(err.message)
        })
    } catch (error) {
        throw Error(error)
    }
}

const SourceService = {

    async generateBlurredSites(userId) {
        try {

            const sourceIndices = await SourceIndices.findAll({
                where: {
                    is_send: true
                }
            })

            if (sourceIndices.length > 0) {

                await postgresC.raw("TRUNCATE unblurred_sites RESTART IDENTITY CASCADE;").then((result) => result[0])

                const indices = []
                for (const index of sourceIndices) {
                    indices.push(index.mng_id)
                }

                /*
                const query = {
                    'query': {
                        'match_all': {}
                    }
                } */

                await mongoC.connect()
                const db = mongoC.db(configM.db)
                const ObjectId = require('mongodb').ObjectId

                const documents = []

                if (indices) {
                    for (const index of indices) {
                        const documentId = ObjectId(index)
                        const document = await db.collection("metaurfm").findOne({ _id: documentId })
                        documents.push(document)
                    }
                }

                /*
                let documents = []
                for (const index of indices) {
                    const document = await elkC.search({
                        index: index, body: query
                    })
                    documents.push(document.body.hits.hits)
                } */

                if (documents) {
                    for (const elements of documents) {

                        for (const el of elements.metaUrfms) {

                            const experimentalSite = el.experimental_site

                            if (experimentalSite.geo_point.latitude && experimentalSite.geo_point.longitude) {
                                await UnBlurredSite.create({
                                    userid: 3,
                                    // indexid: indexid,
                                    docid: elements._id.toString(),
                                    x: experimentalSite.geo_point.longitude,
                                    y: experimentalSite.geo_point.latitude,
                                    blurring_rule: experimentalSite.blurring_rule === 'not provided' || null ? '0.1;0.15' : experimentalSite.blurring_rule,
                                    new_point: true
                                })
                            }
                        }
                    }
                }

                const add_geom_from_x_y = await postgresC.raw(`select add_geom_from_x_y()`)

                const generate_blurred_sites = await postgresC.raw(`select generate_blurred_sites()`)

                const blurred_sites_query = await postgresC.raw(`select unb.indexid, unb.docid, b.geojson from unblurred_sites unb inner join blurred_sites b on unb.id = b.id`)

                const blurred_sites = blurred_sites_query.rows


                for (const elements of documents) {


                    console.log(elements.metaUrfms.length)


                }
                /*
                if (blurred_sites) {
                    for (const site of blurred_sites) {
                        const geojson = JSON.parse(site.geojson)
                        const documentId = ObjectId(site.docid)
                        await db.collection("metaurfm").findOneAndUpdate({ _id: documentId }, {
                            $set: {
                                "experimental_site.site_geojson.geometry.type": geojson.type,
                                "experimental_site.site_geojson.geometry.coordinates": geojson.coordinates
                            }
                        })
                    }
                } */
                // console.log(blurred_sites)
                /*
                if (blurred_sites) {
                    for (const site of blurred_sites) {
                        const geojson = JSON.parse(site.geojson)

                        await elkC.update({
                            index: site.indexid,
                            id: site.docid,
                            body: {
                                doc: {
                                    experimental_site: {
                                        "site_geojson": {
                                            "geometry": {
                                                "type": geojson.type,
                                                "coordinates": geojson.coordinates
                                            }
                                        }
                                    }
                                }
                            }
                        })
                    }
                } */
                return true
            } else {
                throw Error("There is no source!")
            }
        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            console.log(error)
        }
    },

    async create(ctx, header) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            if (ctx.kc_id === undefined)
                throw Error("kcId did not supported.")

            gatekeeperClient.authorization = header.authorization;

            const { content } = await gatekeeperClient.post(`user/detail`, { kcId: ctx.kc_id })

            if (content) {
                await mongoC.connect()

                const db = mongoC.db(configM.db)

                if (ctx.metaUrfms && ctx.name && content) {
                    let metadata = { metaUrfms: ctx.metaUrfms }
                    let ObjectId = require('mongodb').ObjectId

                    const source = await Source.create({
                        name: ctx.name,
                        description: ctx.description
                    })

                    if (source) {
                        await db.collection('metaurfm').insertOne(metadata, function (error, record) { });

                        const sourceIndices = await SourceIndices.create({
                            source_id: source.id,
                            mng_id: ObjectId(metadata._id).toString(),
                        })

                        const sourceProvider = await ProviderSource.create({
                            user_id: content.id,
                            source_id: source.id,
                        })

                        botService.message(`[INFO]:[in-sylva.source.manager]:[USER:${content.username}] created new source with [ID:[${source.id}] & SOURCE_NAME:[${ctx.name}] & MONGODB_ID:[${metadata._id}]] successfully`)
                        return sourceProvider
                    }
                } else {
                    throw Error("Invalid request!")
                }
            } else {
                throw Error("User not found!")
            }
        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        } finally {
            // mongoC.close()
        }
    },

    async sources(ctx, header) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            if (ctx.kc_id === undefined)
                throw Error("kcId did not supported.")

            gatekeeperClient.authorization = header.authorization

            let { content } = await gatekeeperClient.post(`user/detail`, { kcId: ctx.kc_id })
            if (content) {
                const params = { userId: content.id }

                const sources = await postgresC.raw(`
                select s.id, s.name, si.index_id, si.mng_id, s.description, s.createdat, si.is_send from sources s
                    inner join sources_indices si on si.source_id = s.id
                    inner join provider_sources ps on ps.source_id = si.source_id
                                where ps.user_id = ?`, params.userId)

                return sources.rows
            } else {
                throw Error("User not found!")
            }

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async indexedSources(ctx, header) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            if (ctx.kc_id === undefined)
                throw Error("kcId did not supported.")

            gatekeeperClient.authorization = header.authorization

            let { content } = await gatekeeperClient.post(`user/detail`, { kcId: ctx.kc_id })
            if (content) {
                const params = { userId: content.id }

                const sources = await postgresC.raw(`
                select s.id, s.name, si.index_id, si.mng_id, s.description, s.createdat, si.is_send from sources s
                    inner join sources_indices si on si.source_id = s.id
                    inner join provider_sources ps on ps.source_id = si.source_id
                                where si.index_id is not null and ps.user_id = ?`, params.userId)

                return sources.rows
            } else {
                throw Error("User not found!")
            }

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async source_indexes(ctx, header) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            if (ctx.kc_id === undefined)
                throw Error("kcId did not supported.")

            gatekeeperClient.authorization = header.authorization;

            const { content } = await gatekeeperClient.post(`user/detail`, { kcId: ctx.kc_id })
            if (content) {
                var params = { userId: content.id };
                const sources = await postgresC.raw(`
                select s.id,si.index_id, s.name, s.description, s.createdat, si.is_send from sources s
                    inner join sources_indices si on si.source_id = s.id
                    inner join provider_sources ps on ps.source_id = si.source_id
                    where si.is_send = true
                    order by s.createdat desc                
                        `, params)
                // where si.is_send = true and ps.user_id = :userId         
                return sources.rows
            } else {
                throw Error("User not found!")
            }
        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },
    //  wget -qO- --post-data="kc_id=2ce800a9-e7b5-4d7f-af3f-03c0a59db78f&mng_id=5e9484c032cd94002a5a84a1&source_id=25" http://localhost:5000/update_source
    async updateSource(ctx, header) {
        let elkIndex = ""
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            if (ctx.kc_id === undefined)
                throw Error("kcId did not supported.")

            gatekeeperClient.authorization = header.authorization;

            let { content } = await gatekeeperClient.post(`user/detail`, { kcId: ctx.kc_id })

            if (content) {

                let params = { userId: content.id, sourceId: ctx.source_id }

                let source = await postgresC.raw(`
                    select s.name, s.id,si.mng_id from sources s
                        inner join sources_indices si on si.source_id = s.id
                        inner join provider_sources ps on ps.source_id = si.source_id
                            where si.is_send = false and ps.source_id = :sourceId  and ps.user_id = :userId`, params)

                if (!source)
                    throw Error("Interested source did not found.")

                await mongoC.connect();
                const db = mongoC.db(configM.db)

                const ObjectId = require('mongodb').ObjectId;
                const documentId = ObjectId(source.rows[0].mng_id)

                const result = () => {
                    return new Promise((resolve, reject) => {
                        db.collection("metaurfm").findOne({ _id: documentId }, function (error, record) {
                            error
                                ? reject(error)
                                : resolve(record)
                        })
                    })
                }

                let metaurfmDocument = await result()

                if (metaurfmDocument) {

                    elkIndex = `${source.rows[0].name.split(/\s/).join('').toLowerCase()}-${source.rows[0].mng_id}`

                    await elkC.indices.create({
                        index: elkIndex
                    }, { ignore: [400] })

                    const body = metaurfmDocument.metaUrfms.flatMap(doc => [{ index: { _index: elkIndex } }, doc])

                    const { body: bulkResponse } = await elkC.bulk({ refresh: true, body })

                    if (bulkResponse.errors) {
                        const erroredDocuments = []

                        bulkResponse.items.forEach((action, i) => {
                            const operation = Object.keys(action)[0]
                            if (action[operation].error) {
                                erroredDocuments.push({

                                    status: action[operation].status,
                                    error: action[operation].error,
                                    operation: body[i * 2],
                                    document: body[i * 2 + 1]
                                })
                            }
                        })
                        console.log(erroredDocuments)
                    }

                    const { body: count } = await elkC.count({ index: elkIndex })

                    if (count) {

                        const sourceIndices = await SourceIndices.update({
                            is_send: true,
                            index_id: elkIndex
                        }, {
                            where: {
                                source_id: params.sourceId
                            }
                        })

                        return await this.generateBlurredSites(params.userId)
                    }
                }
            } else {
                throw Error("User not found!")
            }

        } catch (error) {
            await deleteElkIndex(elkIndex)
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        } finally {
            // mongoC.close()
        }
    },

    // kcId 
    // SourceId
    async deleteSource(ctx, header) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            gatekeeperClient.authorization = header.authorization;

            const { kc_id, sourceId } = ctx;

            const { content } = await gatekeeperClient.post(`user/detail`, { kcId: kc_id })
            if (content) {

                await mongoC.connect();
                const db = mongoC.db(configM.db)

                const ObjectId = require('mongodb').ObjectId;

                const source = await SourceIndices.findOne({
                    where: {
                        source_id: sourceId
                    }
                })

                if (source.source_id) {
                    await Source.destroy({
                        where: {
                            id: sourceId
                        }
                    })
                }

                if (source.mng_id) {
                    const documentId = ObjectId(source.mng_id)
                    await db.collection("metaurfm").deleteMany({ _id: documentId })
                }

                if (source.index_id) {
                    await deleteElkIndex(source.index_id)
                }

                return true
            } else {
                throw Error("User not found!")
            }
        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async allSource(ctx, header) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const sources = await postgresC.raw(`
            select s.id, s.name, si.index_id, si.mng_id, s.description, s.createdat, si.is_send from sources s
                inner join sources_indices si on si.source_id = s.id
                inner join provider_sources ps on ps.source_id = si.source_id`)

            return sources.rows

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async allIndexedSource(ctx, header) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const sources = await postgresC.raw(`
            select s.id, s.name, si.index_id, si.mng_id, s.description, s.createdat, si.is_send from sources s
                inner join sources_indices si on si.source_id = s.id
                inner join provider_sources ps on ps.source_id = si.source_id 
                where si.index_id is not null`)

            return sources.rows

        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },

    async mergeAndSendSource(ctx, header) {
        try {
            if (!ctx) {
                throw Error("The request body is empty!")
            }

            const { kc_id, name, description, sources } = ctx

            const { content } = await gatekeeperClient.post(`user/detail`, { kcId: kc_id })
            if (kc_id && name && sources && content) {

                await mongoC.connect()
                const db = mongoC.db(configM.db)
                const ObjectId = require('mongodb').ObjectId

                const mng_documents = []
                for (const source of sources) {
                    const documentId = ObjectId(source.mng_id)
                    const document = await db.collection("metaurfm").findOne({ _id: documentId })
                    mng_documents.push([...document.metaUrfms])
                }

                const lastIndex = mng_documents.length
                const check_mng_documents = []
                for (let i = 0; i < mng_documents.length; i++) {
                    if (i + 1 < lastIndex) {
                        const isEqual = JSON.stringify(mng_documents[i]) == JSON.stringify(mng_documents[i + 1])
                        if (isEqual) {
                            check_mng_documents.push({ index: i + 1, isEqual: true })
                        }
                    }
                }

                if (check_mng_documents.length > 0) {
                    throw Error("You can't merge same documents!")
                }

                const elk_documents = { metaUrfms: [] }
                for (const xDocs of mng_documents) {
                    for (const xdoc of xDocs) {
                        elk_documents.metaUrfms.push(xdoc)
                    }
                }

                if (elk_documents) {
                    const uuid = require('uuid')
                    const elkIndex = `${name.toLowerCase()}-merged-${uuid.v4()}`

                    await elkC.indices.create({
                        index: elkIndex
                    }, { ignore: [400] })

                    const body = elk_documents.metaUrfms.flatMap(doc => [{ index: { _index: elkIndex } }, doc])
                    const { body: bulkResponse } = await elkC.bulk({ refresh: true, body })

                    if (bulkResponse.errors) {
                        const erroredDocuments = []

                        bulkResponse.items.forEach((action, i) => {
                            const operation = Object.keys(action)[0]
                            if (action[operation].error) {
                                erroredDocuments.push({
                                    status: action[operation].status,
                                    error: action[operation].error,
                                    operation: body[i * 2],
                                    document: body[i * 2 + 1]
                                })
                            }
                        })

                        console.log(erroredDocuments)
                    }

                    const { body: count } = await elkC.count({ index: elkIndex })

                    if (count) {
                        const newSource = await Source.create({
                            name: name,
                            description: description == (null || undefined) ? "source description" : description,
                        })

                        const newSourceProvider = await ProviderSource.create({
                            source_id: newSource.id,
                            user_id: content.id
                        })

                        const newSourceIndices = await SourceIndices.create({
                            source_id: newSource.id,
                            is_send: true,
                            index_id: elkIndex
                        })

                        await this.generateBlurredSites(content.id)

                        return { newSource, newSourceProvider, newSourceIndices, status: 201 }
                    }
                }
            } else {
                throw Error("User not found!")
            }
        } catch (error) {
            botService.message(`Caught: ${error} from in-sylva.source.manager`)
            throw Error(error)
        }
    },
}

module.exports = {
    create: SourceService.create,
    update: SourceService.updateSource,
    sources: SourceService.sources,
    indexedSources: SourceService.indexedSources,
    allSource: SourceService.allSource,
    allIndexedSource: SourceService.allIndexedSource,
    source_indexes: SourceService.source_indexes,
    delete: SourceService.deleteSource,
    mergeAndSendSource: SourceService.mergeAndSendSource,
    generateBlurredSites: SourceService.generateBlurredSites
}




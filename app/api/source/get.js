'use strict'

const SourceService = require("../../dal/sourceService")
const logger = require('../../utils/format')
const util = require('util')

const Source = {

    async allSource(ctx) {
        try {
            ctx.body = await SourceService.allSource(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('sources listed successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async allIndexedSource(ctx) {
        try {
            ctx.body = await SourceService.allIndexedSource(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('sources listed successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    }
}


module.exports = {
    allSource: Source.allSource,
    allIndexedSource: Source.allIndexedSource,
    Source
}
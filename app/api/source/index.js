'use strict'

const { sources, indexedSources, source_indexes, addSource, updateSource, deleteSource, mergeAndSendSource } = require("./post")
const { allSource, allIndexedSource } = require('./get')
const SourceHandler = {
    addSource,
    sources,
    indexedSources,
    source_indexes,
    updateSource,
    allSource,
    allIndexedSource,
    deleteSource,
    mergeAndSendSource
}


module.exports = SourceHandler


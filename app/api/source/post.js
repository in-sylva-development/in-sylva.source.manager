'use strict'

const SourceService = require("../../dal/sourceService")
const logger = require('../../utils/format')
const util = require('util')


const Source = {

    async sources(ctx) {
        try {
            ctx.body = await SourceService.sources(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('sources listed successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async indexedSources(ctx) {
        try {
            ctx.body = await SourceService.indexedSources(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('sources listed successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async source_indexes(ctx) {
        try {
            ctx.body = await SourceService.source_indexes(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('sources listed successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async addSource(ctx) {
        try {
            ctx.body = await SourceService.create(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('metadata is added to mongodb successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async updateSource(ctx) {
        try {
            ctx.body = await SourceService.update(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('metadata is updated successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async deleteSource(ctx) {
        try {
            ctx.body = await SourceService.delete(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('Source is deleted successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async mergeAndSendSource(ctx) {
        try {
            ctx.body = await SourceService.mergeAndSendSource(ctx.request.body, ctx.request.header)
            ctx.status = 201
            logger.info('Sources are merged and sent successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    }

}

module.exports = {
    sources: Source.sources,
    indexedSources: Source.indexedSources,
    source_indexes: Source.source_indexes,
    addSource: Source.addSource,
    updateSource: Source.updateSource,
    deleteSource: Source.deleteSource,
    mergeAndSendSource: Source.mergeAndSendSource,
    Source
}
'use strict'

const Router = require("koa-router")
const bodyParser = require("koa-bodyparser")

const { keycloak } = require('../../init-keycloak')

// handlers
const sourceHandler = require('./source')
const fieldHandler = require('./fields')

const routers = new Router()

routers.use(bodyParser({ formLimit: "700mb", jsonLimit: "700mb", textLimit: "700mb" }))
routers.get('/healthcheck', (ctx) => {
    ctx.status = 201
    ctx.body = 'in-sylva.source.manager is up!'
})

// source 
routers.post('/source', /* keycloak.protect(), */ sourceHandler.addSource)
routers.post('/sources',/* keycloak.protect(), */ sourceHandler.sources)
routers.post('/indexed-sources',/* keycloak.protect(), */ sourceHandler.indexedSources)
routers.get('/allSource', sourceHandler.allSource)
routers.get('/allIndexedSource', sourceHandler.allIndexedSource)
routers.post('/source_indexes',/* keycloak.protect(), */ sourceHandler.source_indexes)
routers.post('/update_source',/* keycloak.protect(), */sourceHandler.updateSource)
routers.post('/source/delete', sourceHandler.deleteSource)
routers.post('/source/merge-and-send', sourceHandler.mergeAndSendSource)
// field
routers.post('/addStdField',/* keycloak.protect(),*/ fieldHandler.addStdField)
routers.post('/addAddtlField',/* keycloak.protect(),*/ fieldHandler.addAddtlField)
routers.post('/updateStdField', /* keycloak.protect(),*/ fieldHandler.updateStdField)
routers.get('/stdFields', /* keycloak.protect(),*/ fieldHandler.stdFields)
routers.get('/privateFieldList', fieldHandler.privateFieldList)
routers.get('/publicFieldList', fieldHandler.publicFieldList)
routers.post('/policy-stdfields', fieldHandler.policyFields)
routers.delete('/stdFields/truncate', fieldHandler.truncateStdField)

// field_specifications

module.exports = routers
'use strict'

const FieldService = require("../../dal/fieldService")
const logger = require('../../utils/format')
const util = require('util')

const Field = {

    async stdFields(ctx) {
        try {
            ctx.body = await FieldService.stdFields(ctx.request.body)
            ctx.status = 201
            logger.info('Standard fields are listed successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught error: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },
    async privateFieldList(ctx) {
        try {
            ctx.body = await FieldService.privateFieldList(ctx.request.body)
            ctx.status = 201
            logger.info('Standard fields are listed successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught error: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },
    async publicFieldList(ctx) {
        try {
            ctx.body = await FieldService.publicFieldList(ctx.request.body)
            ctx.status = 201
            logger.info('Standard fields are listed successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught error: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },
    async policyFields(ctx) {
        try {
            ctx.body = await FieldService.policyFields(ctx.request.body)
            ctx.status = 201
            logger.info('Policy Std_Fields (private) listed successfully!')
        } catch (err) {
            ctx.body = err || 'error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    }
};

module.exports = {
    stdFields: Field.stdFields,
    privateFieldList: Field.privateFieldList,
    publicFieldList: Field.publicFieldList,
    policyFields: Field.policyFields
}
'use strict'

const { addStdField, addAddtlField, updateStdField, truncateStdField } = require('./post')
const { stdFields, privateFieldList, publicFieldList, policyFields } = require('./get')

const FieldHandler = {
    addStdField, addAddtlField, updateStdField, stdFields, privateFieldList, publicFieldList, policyFields, truncateStdField
}

module.exports = FieldHandler
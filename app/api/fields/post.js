'use strict'

const FieldService = require("../../dal/fieldService")
const logger = require('../../utils/format')
const util = require('util')

const Field = {

    async addStdField(ctx) {
        try {
            ctx.body = await FieldService.createStd(ctx.request.body)
            ctx.status = 201
            logger.info('Standard field is added to successfully!')
        } catch (err) {
            ctx.body = err || 'Error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async updateStdField(ctx) {
        try {
            ctx.body = await FieldService.updateStd(ctx.request.body)
            ctx.status = 201
            logger.info('Standard field is updated. to successfully!')
        } catch (err) {
            ctx.body = err || 'error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async addAddtlField(ctx) {
        try {
            ctx.body = await FieldService.createAddtl(ctx.request.body)
            ctx.status = 201
            logger.info('Standard field is updated. to successfully!')
        } catch (err) {
            ctx.body = err || 'error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    },

    async truncateStdField(ctx) {
        try {
            ctx.body = await FieldService.truncateStdField(ctx.request.body)
            ctx.status = 201
            logger.info('Standard fields are truncated successfully!')
        } catch (err) {
            ctx.body = err || 'error occurred!'
            ctx.status = 500
            logger.error(`Caught: ${JSON.stringify(util.inspect(err, { compact: false, depth: 1, breakLength: 80 }))}`)
        }
    }
};

module.exports = {
    addStdField: Field.addStdField,
    updateStdField: Field.updateStdField,
    addAddtlField: Field.updateStdField,
    truncateStdField: Field.truncateStdField
}

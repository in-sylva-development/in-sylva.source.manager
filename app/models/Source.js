
'use strict'

const { postgresSeq } = require('../../db')
const Sequelize = require('sequelize')

class Source extends Sequelize.Model { }

Source.init({
    name: { type: Sequelize.STRING, allowNull: false },
    description: { type: Sequelize.TEXT, allowNull: true }
}, {
    underscored: true,
    sequelize: postgresSeq,
    modelName: "sources"
})

class SourceIndices extends Sequelize.Model { }

SourceIndices.init({
    source_id: {
        type: Sequelize.INTEGER, allowNull: true, references: {
            model: Source,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    index_id: { type: Sequelize.STRING, allowNull: true },
    mng_id: { type: Sequelize.STRING, allowNull: true },
    is_send: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: false }
}, {
    underscored: true,
    sequelize: postgresSeq,
    modelName: "sources_indices"
})

const Indices = Source.hasMany(SourceIndices, { as: 'indices' })

module.exports = {
    Source,
    SourceIndices,
    Indices
}
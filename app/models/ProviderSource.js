'use strict'

const { postgresSeq } = require('../../db')
const Sequelize = require('sequelize')
const { Source } = require('../../app/models/Source')
const { User } = require('../../app/models/User')

class ProviderSource extends Sequelize.Model { }

ProviderSource.init({
    source_id: {
        type: Sequelize.INTEGER, allowNull: true, references: {
            model: Source,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    user_id: {
        type: Sequelize.INTEGER, allowNull: true, references: {
            model: User,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    }
}, {
    undcored: true,
    sequelize: postgresSeq,
    modelName: "provider_sources"
})

User.hasMany(ProviderSource)
Source.hasMany(ProviderSource)

module.exports = {
    ProviderSource
}

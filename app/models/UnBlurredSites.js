'use strict'

const { postgresSeq } = require('../../db')
const Sequelize = require('sequelize')
const { User } = require('../../app/models/User')


class UnBlurredSite extends Sequelize.Model { }

UnBlurredSite.init({
    userid: {
        type: Sequelize.INTEGER, allowNull: false
    },
    indexid: {
        type: Sequelize.STRING, allowNull: true
    },
    docid: {
        type: Sequelize.STRING, allowNull: true
    },
    siteid: {
        type: Sequelize.INTEGER, allowNull: true
    },
    x: {
        type: Sequelize.REAL, allowNull: false
    },
    y: {
        type: Sequelize.REAL, allowNull: false
    },
    geom: {
        type: Sequelize.GEOMETRY, allowNull: true
    },
    blurring_rule: {
        type: Sequelize.CHAR(30), allowNull: true
    },
    new_point: {
        type: Sequelize.BOOLEAN, allowNull: true
    }
}, {
    undcored: true,
    sequelize: postgresSeq,
    modelName: "unblurred_sites"
})


module.exports = {
    UnBlurredSite
}
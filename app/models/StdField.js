'use strict'

const { postgresSeq } = require('../../db')
const Sequelize = require('sequelize')


class StdField extends Sequelize.Model { }

StdField.init({
    std_field_id: {
        type: Sequelize.INTEGER, allowNull: true,
        references: {
            model: StdField,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    category: { type: Sequelize.STRING, allowNull: true },
    field_name: { type: Sequelize.STRING, allowNull: true },
    definition_and_comment: { type: Sequelize.TEXT, allowNull: true },
    obligation_or_condition: { type: Sequelize.TEXT, allowNull: true },
    cardinality: { type: Sequelize.STRING, allowNull: true },
    field_type: { type: Sequelize.STRING, allowNull: true },
    values: { type: Sequelize.STRING, allowNull: true },
    ispublic: { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: true }, 
    isoptional:  { type: Sequelize.BOOLEAN, allowNull: false, defaultValue: true }, 
},{
    underscored: true,
    sequelize: postgresSeq,
    modelName: "std_fields"
})
/*
class StdFieldValue extends Sequelize.Model {} 

StdFieldValue.init({
    std_field_id: {
        type: Sequelize.INTEGER, allowNull: true,
        references: {
            model: StdField,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
        }
    },
    values: { type: Sequelize.STRING, allowNull: true }
}) */


module.exports = {
    StdField,
    //StdFieldValue
}
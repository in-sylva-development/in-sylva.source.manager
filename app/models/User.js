'use strict'

const { postgresSeq } = require('../../db')
const Sequelize = require('sequelize')

class User extends Sequelize.Model { }

User.init({
    kc_id: { type: Sequelize.STRING, allowNull: false, unique: 'compositeIndex' },
    username: { type: Sequelize.STRING, allowNull: false, unique: 'compositeIndex' },
    name: { type: Sequelize.STRING, allowNull: false },
    surname: { type: Sequelize.STRING, allowNull: false },
    email: { type: Sequelize.STRING, allowNull: false, validate: {
        isEmail: true
    } },
    password: { type: Sequelize.STRING, allowNull: false }
}, {
    underscored: true,
    sequelize: postgresSeq,
    modelName: 'user'
})

module.exports = {
    User
}
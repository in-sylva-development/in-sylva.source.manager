'use strict'; 

const elk = {
    host:process.env.ELK_HOST,
    port: process.env.ELK_PORT,
    username: process.env.ELK_USERNAME,
    password: process.env.ELK_PASSWORD,
}

module.exports = elk


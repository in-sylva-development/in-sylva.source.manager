'use strict';

const mongo = {
    port: process.env.MONGO_PORT,
    host: process.env.MONGO_HOST,
    db: process.env.MONGO_DB_NAME,
    username: process.env.MONGO_USERNAME,
    password: process.env.MONGO_PASSWORD
}

module.exports = mongo;
'use strict'

const dotenv = require('dotenv');

if (process.env.NODE_ENV === 'development') {
    dotenv.config();
}

const processType = process.env.PROCESS_TYPE

switch (processType) {
    case 'web':
        require('./web')
        break
    case 'script':
        require('./migrate')
        break
    default:
        throw new Error(
            `Invalid process type: ${processType}. It should be one of: 'web', 'script'.`
        )
}